package com.progressoft.binarytree;

public interface TreeState {
    boolean accept(int value);
    int depth();
    int nodeDepth(int value);

    void print();
}
