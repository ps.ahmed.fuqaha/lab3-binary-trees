package com.progressoft.binarytree;

public class SDPbinaryTree {

    private TreeState state;
    private Node root;

    public SDPbinaryTree() {
        root = new Node();
        state = new EmptyState();
    }

    public boolean accept(int value) {
        return state.accept(value);
    }

    public int depth() {
        return state.depth();
    }

    public int nodeDepth(int value) {
        return state.nodeDepth(value);
    }

    public void print() {
        state.print();
    }

    private class EmptyState implements TreeState {
        @Override
        public boolean accept(int value) {
            root = new Node(value);
            state = new NonEmptyState();
            return true;
        }

        @Override
        public int depth() {
            return 0;
        }

        @Override
        public int nodeDepth(int value) {
            throw new IllegalStateException("tree is empty");
        }

        public void print() {
            System.out.println("EMPTY");
        }
    }

    private class NonEmptyState implements TreeState {

        @Override
        public boolean accept(int value) {
            return root.accept(value);
        }

        @Override
        public int depth() {
            return root.depth();
        }

        @Override
        public int nodeDepth(int value) {
            return root.depth(value);
        }

        public void print() {
            System.out.println("NON EMPTY");
        }
    }
}
