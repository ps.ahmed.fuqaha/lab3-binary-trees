package com.progressoft.binarytree;

public class BinaryTree {
    // TODO state design pattern (using inner classes and interfaces)
    private Node root;

    public boolean accept(int value) {
        if (isEmpty()) {
            root = new Node(value);
            return true;
        }
        return root.accept(value);

    }

    public int nodeDepth(int value) {
        if (isEmpty())
            throw new IllegalStateException("tree is empty");
        return root.depth(value);
    }

    public int treeDepth(Node root) {
        if (isEmpty())
            return 0;
        return root.depth();
    }


    private boolean isEmpty() {
        return root == null;
    }
}
