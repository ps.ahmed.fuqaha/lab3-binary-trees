package com.progressoft.binarytree;

import java.util.function.Consumer;

public class Node {
    int data;
    private Node left;
    private Node right;

    public Node() {
    }

    public int depth(int value) {
        if (alreadyExists(value)) {
            return 1;
        }
        int depth = -1;
        if (isLeaf()) {
            return -1;
        }
        if (checkRight(value) && right != null) {
            depth = right.depth(value);
        }
        if (left != null) {
            depth = left.depth(value);
        }
        return depth == -1 ? -1 : depth + 1;
    }

    public boolean isLeaf() {
        return left == null && right == null;
    }

    public Node(int data) {
        this.data = data;
    }

    public boolean accept(int value) {
        if (alreadyExists(value))
            return false;
        if (checkRight(value))
            return acceptToRight(value);
        return acceptToLeft(value);
    }

    private boolean acceptToLeft(int value) {
        return accept(value, left, n -> this.left = n);
    }

    private boolean acceptToRight(int value) {
        return accept(value, right, n -> this.right = n);
    }

    private boolean accept(int value, Node node, Consumer<Node> assignment) {
        if (emptyNode(node)) {
            node = new Node(value);
            assignment.accept(node);
            return true;
        }
        return node.accept(value);
    }


    private boolean checkRight(int value) {
        return value > data;
    }

    private boolean alreadyExists(int value) {
        return data == value;
    }

    private boolean emptyNode(Node node) {
        return node == null;
    }

    public int depth() {
        if (isLeaf())
            return 1;
        return Math.max(rightDepth(), leftDepth());
    }

    private int leftDepth() {
        return childDepth(left);
    }

    private int rightDepth() {
        return childDepth(right);
    }

    private int childDepth(Node node) {
        return node == null ? 0 : node.depth();
    }
}
