package com.progressoft;

import com.progressoft.binarytree.SDPbinaryTree;

public class Main {
    public static void main(String[] args) {
        SDPbinaryTree tree = new SDPbinaryTree();
        tree.print();
        tree.accept(5);
        tree.print();
    }
}